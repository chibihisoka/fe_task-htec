// define default project object
var FETask = {
	ajax : {},
	events : {
		carAnimationParams: {
				covered_distance : 0,
				speed_limit : 0
		}
	},
	parsers : {},
	storage : {},
	interupts: {},
	timers : {
		traficLights: {},
		traficLightRed: {},
		fast : 250,
		medium : 400,
		slow : 600
	}
}

// ajax method
FETask.ajax.get = function( params ){
    var rawFile = new XMLHttpRequest(); // create new HTTP request
    rawFile.open("GET", params.endpoint, true); // set method and endpoint for request
    rawFile.onreadystatechange = function (){ // detect ready change
        if(rawFile.readyState === 4){ // if done proceed
            if(rawFile.status === 200 || rawFile.status == 0){ //if success
                if ( typeof params.callback === "function" ){ // if there is callback for ajax
                	FETask.storage.appData = JSON.parse(this.response); // store recieved data in local object variable
                	var delegate = { response : FETask.storage.appData }; // construct object for callback function
					if ( params.callbackParams != "" ){ // if there are additional params for ajax
						delegate.params = params.callbackParams;  // pass additional parameters for callback function
					}
					params.callback( delegate );  // invoke callback function
				}
            }
        }
    }
    rawFile.send(null); // start http request
}

/*=================================================================*/
/*===========================  parsers  ===========================*/
/*=================================================================*/

FETask.parsers.parseInitialResponse = function( data ){
	var localData = false, localParams = false; // create local variables
	if ( typeof data.response == "object" ) localData = data.response; // assign value to variable if value is object
	if ( typeof data.params == "object" ) localParams = data.params; // assign value to variable if value is object
	
	FETask.parsers.createVehicleOverviewCards( localData.cars );
	FETask.events.carAnimation( localData ); // passing json data to car animation function
}

// parse HTML elements
FETask.parsers.createVehicleOverviewCards = function( data ){
	var html = "";
	
	// create search field
	html += FETask.parsers.searchFieldHTML();
	
	// go through data and create HTML for each element
	if ( data.length ){
		html += "<div id='cars-wrapper' class='car-list-wrapper'>";
		for ( var key in data ){
			html += FETask.parsers.createVehicleOverviewCardHTML( data[key] );
		}
		html += "</div>";
	}
	
	// create raod element
	html += FETask.parsers.roadElementHTML();
	
	// create animation button and input
	html += FETask.parsers.createAnimationStartElementsHTML();
	
	var appWrapper = document.getElementById("main-wrapper");
	appWrapper.innerHTML = html;
	
	// bind search field events (click, keypress)
	FETask.events.bindEvents();
	
	// bind trafic lights events
	FETask.events.traficLight();
	
	// bind animation speed input events
	FETask.events.animationSpeedInoutCheck();

	// bind car animation button click
	FETask.events.bindCarAnimationEvents();
}

// create singe card for vehicle data
FETask.parsers.createVehicleOverviewCardHTML = function( vehicle ){
	var html = "";
	html += "<div class='car-list-item'>";
		html += "<div class='car-list-item-wrapper' data-img='"+ vehicle.image +"'data-speed='" + vehicle.speed + "'>";
			html += "<div class='car-list-item-container'>";
				html += "<img src='"+ vehicle.image +"' alt='"+ vehicle.name +"' />";
				html += "<p class='car-name'>"+ vehicle.name +"</p>";
			html += "</div>";
			html += "<div class='car-list-item-backface'>";
				html += "<div class='car-information'>";
					html += "<p class='car-description'>"+ vehicle.description +"</p>";
					html += "<p class='car-speed'>"+ vehicle.speed +"</p>";
				html += "</div>";
			html += "</div>";
			html += "<label>";
				html += "<input type='checkbox' class='car-select' />";
				html += "<span></span>";
			html += "</label>";
		html += "</div>";
	html += "</div>";
	return html;
}

// create search input
FETask.parsers.searchFieldHTML = function(){
	var html = "";
	html += "<form action='' class='search-wrapper'>";
		html += "<input id='search-field' type='search' />";
		html += "<button id='search-btn' type='button'><img src='https://image.flaticon.com/icons/svg/49/49116.svg' alt='' /></button>";
	html += "</form>";
	return html;
}

// create road element
FETask.parsers.roadElementHTML = function(){
	var html = "";
	
	FETask.storage.distanceMultiplier = 100 / parseInt(FETask.storage.appData.distance);
	
	html += "<div class='raod-outer-wrapper'>";
		// road markings in kilometers
		html += FETask.parsers.createRoadMarkingsHTML();
		
		// road element
		html += FETask.parsers.createRoadSegmentsHTML();
		
		// road signs elements
		html += FETask.parsers.createRoadSignsHTML( FETask.storage.appData.speed_limits );
		
		// trafic light
		html += FETask.parsers.createTraficLightHTML( FETask.storage.appData.traffic_lights );
	html += "</div>";
	
	return html;
}

// create raod markings HTML 
FETask.parsers.createRoadMarkingsHTML = function(){
	var html = "";
	
	html += "<div class='raod-distance-marking-container'>";
		for ( var n = 1; n < 10; n++ ){
			html += "<div class='raod-distance-marking'>";
				html += "<span>"+ n +"x5km</span>";
			html += "</div>";
		}
	html += "</div>";
	
	return html;
}

// create road segment HTML
FETask.parsers.createRoadSegmentsHTML = function (){
	var html = "";
	
	html += "<div class='raod-element-wrapper'>";
		for ( var i = 0; i < 3; i++ ){
			html += "<div class='raod-element'>";
				html += "<div class='car-position'></div>";
				for ( var k = 0; k < 10; k++ ){
					html += "<div class='raod-element-segment'></div>";
				}
			html += "</div>";
		}
	html += "</div>";
	
	return html;
}

// create road signs HTML
FETask.parsers.createRoadSignsHTML = function( data ){
	var html = "";
	
	for ( var x = 0; x < data.length; x++ ){
		html += "<div class='road-sign-wrapper' style='left: "+ parseInt(data[x].position)*FETask.storage.distanceMultiplier +"%'>";
			html += "<div class='road-sign-position-marker'></div>";
			html += "<div class='road-sign-speed-limit-wrapper'>";
				html += "<span>"+ data[x].speed +"</span>";
			html += "</div>";
		html += "</div>";
	}
	
	return html;
}

// create trafic light HTML
FETask.parsers.createTraficLightHTML = function( data ){
	var html = "";
	
	for ( var i = 0; i < data.length; i++ ){
		html += "<div class='trafic-light-wrapper' style='left: "+ parseInt(data[i].position)*FETask.storage.distanceMultiplier +"%' data-interval='"+ data[i].duration +"'>";
			html += "<div class='trafic-light-position-marker'></div>";
			html += "<div class='trafic-light-element-wrapper'>";
				html += "<div class='red-light active'></div>";
				html += "<div class='green-light'></div>";
			html += "</div>";
		html += "</div>";
	}
	
	return html;
}

// create car icon on the road
FETask.parsers.createCarsOnRoad = function( data ){
	var html = "";
	
	html += "<div class='car-icon-wrapper' data-speed='" + data.speed + "'>"
		html += "<img src='"+ data.img +"' />";
	html += "</div>";	
	
	return html;
}

// create button for animation start and input for snimation speed
FETask.parsers.createAnimationStartElementsHTML = function(){
	var html = "";
	
	html += "<div class='animation-start-wrapper'>";
		html += "<button id='animationStart' type='button' disabled='disabled'>Start</button>";
		html += "<input id='animationSpeed' type='text' placeholder='Animation speed in miliseconds' />";
	html += "</div>";
	
	return html;
}


// filter search results
FETask.parsers.filterSearchResults = function ( data ){
	var carsWrapper = document.getElementById("cars-wrapper");
	var carsCollection = carsWrapper.getElementsByClassName("car-list-item");
	// loop through carCollection
	for ( var i = 0; i < carsCollection.length; i++ ){
		var car = carsCollection[i];
		var carName = car.getElementsByClassName("car-name")[0].textContent;
		carName = carName.toLowerCase();
		if ( carName.indexOf(data.search) > -1 ){
			// current valid - remove class for hideing
			car.getElementsByClassName("car-list-item-container")[0].classList.remove("hidden");
			
		} else {
			// current invalid - add class for hideing
			car.getElementsByClassName("car-list-item-container")[0].classList.add("hidden");
		}
	}
}

// show all entries - search field empty
FETask.parsers.showAllResults = function(){
	var carsWrapper = document.getElementById("cars-wrapper");
	var carsCollection = carsWrapper.getElementsByClassName("car-list-item");
	// loop through carCollection
	for ( var i = 0; i < carsCollection.length; i++ ){
		carsCollection[i].getElementsByClassName("car-list-item-container")[0].classList.remove("hidden");
	}
}


/*================================================================*/
/*===========================  events  ===========================*/
/*================================================================*/

// bind all events for parsed HTML
FETask.events.bindEvents = function(){
	FETask.events.bindSearchEvents();
	FETask.events.bindCheckboxEvent();
}

// search events bind
FETask.events.searchTimeout = false;
FETask.events.bindSearchEvents = function(){
	// find search field
	var filter = document.getElementById("search-field");
	// find search button
	var button = document.getElementById("search-btn");
	
	// bind search button click
	button.addEventListener("click", function(){
		if ( typeof FETask.events.searchTimeout == "function" ){
			clearTimeout(FETask.events.searchTimeout);
		} else {
			FETask.events.searchTimeout = false;
		}
		var searchVal = filter.value.toLowerCase();
		if ( searchVal != "" ) {
			FETask.parsers.filterSearchResults({ search: searchVal });
		} else {
			FETask.parsers.showAllResults();
		}
	});
	
	filter.addEventListener("keyup", function( event ){
		var searchVal = this.value.toLowerCase();
		clearTimeout(FETask.events.searchTimeout);
		if ( event.keyCode === 13 && searchVal != "" ) {
			FETask.parsers.filterSearchResults({ search: searchVal });
		} else {
			FETask.events.searchTimeout = setTimeout(function(){
				if ( searchVal != "" ) {
					FETask.parsers.filterSearchResults({ search: searchVal });
				} else {
					FETask.parsers.showAllResults();
				}
			}, FETask.timers.slow);
		}
		
	});
}

// checkbox selection of the car card
FETask.events.bindCheckboxEvent = function(){
	var selectCar = document.getElementsByClassName("car-select");
	for ( var i = 0; i < selectCar.length; i++ ) {
	    selectCar[i].addEventListener("click", function(){
	    	var firstAncestor = this.parentElement;
	    	var wrapperElem = firstAncestor.parentElement;
	    	var numberOfSelectedCars = document.getElementsByClassName("selectedCar");
			if ( this.checked && numberOfSelectedCars.length < 3 ){
				wrapperElem.classList.add("selectedCar");
				FETask.parsers.insertCarToRoad({ car: this.closest(".car-list-item-wrapper") });
			} else {
				wrapperElem.classList.remove("selectedCar");
				this.checked = false;
				FETask.parsers.removeCarFromRoad({ car: this.closest(".car-list-item-wrapper") });
			}
		});
	}
}

// trafic light functionality
FETask.events.traficLight = function(){
	var traficLights = document.getElementsByClassName("trafic-light-wrapper");
	for ( var i = 0; i < traficLights.length; i++ ){
		var redLight = traficLights[i].getElementsByClassName("red-light");
		var greenLight = traficLights[i].getElementsByClassName("green-light");
		var interval = parseInt(traficLights[i].dataset.interval);
		FETask.timers.traficLightRed[i] = true;
		FETask.timers.traficLights[i] = false;
		FETask.events.traficLightSwitch({ red: redLight[0], green: greenLight[0], interval: interval, index: i });
	}
}

// trafic lights interval switch
FETask.events.traficLightSwitch = function( data ){
	//clearInterval(FETask.timers.traficLights[data.index]);
	FETask.timers.traficLights[data.index] = setInterval(function(){
		
		if ( FETask.timers.traficLightRed[data.index] ){
			data.red.classList.remove("active");
			data.green.classList.add("active");
			FETask.timers.traficLightRed[data.index] = false;
		} else {
			data.red.classList.add("active");
			data.green.classList.remove("active");
			FETask.timers.traficLightRed[data.index] = true;
		}
		
	}, data.interval);
}

// insert car in road
FETask.parsers.insertCarToRoad = function( data ){
	var carImg = data.car.dataset.img;
	var speed = data.car.dataset.speed;
	var html = "";
	var raodElem = document.getElementsByClassName("raod-element");
	
	// create html image for selected car
	html += FETask.parsers.createCarsOnRoad({ img: carImg, speed: speed });
	
	// insert car image to road
	for ( var i = 0; i < raodElem.length; i++ ){
		var carIcons = raodElem[i].getElementsByClassName("car-icon-wrapper");
		if ( carIcons.length <= 0 ){
			raodElem[i].innerHTML += html;
			return false;
		}
	}
}

// remove car icons from road
FETask.parsers.removeCarFromRoad = function( data ){
	var carImg = data.car.dataset.img;
	var raodElem = document.getElementsByClassName("raod-element");
	for ( var i = 0; i < raodElem.length; i++ ){
		var carIcons = raodElem[i].getElementsByClassName("car-icon-wrapper");
		if ( carIcons.length > 0 ){			
			var img = carIcons[0].getElementsByTagName("img");
			var imgSrc = img[0].src;
			if ( imgSrc === carImg ) {
				carIcons[0].remove();
				return false;
			}
		}
	}
}

// animation speed input check for type of input abd button enable/disable logic
FETask.events.animationSpeedInoutTypeCheck = function(){
	var animationSpeed = document.getElementById("animationSpeed");
	var animationButton = document.getElementById("animationStart");
	animationSpeed.addEventListener("keyup", function(){
		var inputVal = this.value;
		var inputValParsed = parseInt(this.value);
		
		if ( isNaN(inputValParsed) || inputValParsed === 'NaN' || inputValParsed === "null" || inputValParsed === 'undefind' || inputVal != inputValParsed ){
			animationButton.setAttribute("disabled", "disabled");
		} else {
			animationButton.removeAttribute("disabled");
		}
	});
}

// bind animation button click
FETask.events.bindCarAnimationEvents = function(){
	var button = document.getElementById("animationStart");
	button.addEventListener("click", function(){

		// get value of animation duration in miliseconds
		var animationDuration = parseInt(document.getElementById("animationSpeed").value);

		if( !isNaN(animationDuration) && animationDuration > 0 ){
			FETask.events.prepareCarAnimation({speed: animationDuration});
		} else {
			return false;
		}
	});
}

// animation speed input check for type of input abd button enable/disable logic
FETask.events.animationSpeedInoutCheck = function(){
	var animationSpeed = document.getElementById("animationSpeed");
	var animationButton = document.getElementById("animationStart");
	animationSpeed.addEventListener("keyup", function(e){
		var inputVal = this.value;
		if(inputVal != ''){
			animationButton.removeAttribute("disabled");
			var val = inputVal.replace(/[^0-9.]/g,'');
			this.value = val;
			if (val != ''){
				animationButton.removeAttribute("disabled");
				if(e.keyCode == 13){
					// fire animation
				} else {
					return false;
				}
			} else {
				animationButton.setAttribute("disabled", "disabled");
				return false;
			}
		} else {
			animationButton.setAttribute("disabled", "disabled");
			return false;
		}
	});
}

FETask.events.prepareCarAnimation = function( data ){

	// create array of vehicles to be animated
	var vehiclesForAnimation = [];

	// get all road elements
	var carRoadElement = document.getElementsByClassName("raod-element");

	// select only road elements that have car selected in them
	for(var i = 0; i < carRoadElement.length; i++){
		var carIcon = carRoadElement[i].getElementsByClassName('car-icon-wrapper');
		if(carIcon.length > 0){
			var speed = carIcon[0].dataset.speed;
			vehiclesForAnimation.push( { vehicle: carIcon[0], speed: speed } );
		}
	}

	data.vehicles = vehiclesForAnimation;
	// call animation function
}

// car animation
FETask.events.carAnimation = function( data ){
	// loop through json data and assign speed limits to interupts
	for ( var i = 0; i < data.speed_limits.length; i++ ){
		FETask.interupts[data.speed_limits[i].position] = "speed_limits--" + data.speed_limits[i].speed;
	}
	// loop through json data and assign trafic light position to interupts
	for ( var j = 0; j < data.traffic_lights.length; j++ ){
		FETask.interupts[data.traffic_lights[j].position] = "traffic_lights--" + j;
	}
}

// get json
//startup
window.onload = function(){
	FETask.ajax.get({
		endpoint : "data/data1.json",
		data : {},
		callback : FETask.parsers.parseInitialResponse,
		callbackParams : ""
	});
}

// Make all methods accesible
window.FETask = FETask;